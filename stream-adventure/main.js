var fs = require('fs');

var meet_pipe = function() {
try {
    fs.createReadStream('data.txt').pipe(process.stderr);
} catch (error) {
    console.log("Got exception " + error);
}

var fileName = process.argv[2];
console.log("File got " + fileName);

}

var in_out = function() {
    process.stdin.pipe(process.stdout);
}

var transform = function() {
    var tr_module = require("through");
    var tr = tr_module( function (buff) {
        this.queue(String(buff).toUpperCase());
    } , function () {
        this.queue(null);
    });

    process.stdin.pipe(tr).pipe(process.stdout);
}
var lines = function() {
    var tr_mod = require("through");
    var split_mod = require("split");
    var tr = tr_mod(function(buff) {
            if (this.curr_line % 2 == 1) {
                this.queue(buff.toString().toUpperCase() + "\n");
            } else {
                this.queue(buff + "\n");
            }
            this.curr_line++;
    }, function() { this.queue(null);});
    tr.curr_line = 0;
    var spl = split_mod();

    process.stdin.pipe(spl).pipe(tr).pipe(process.stdout);
    //process.stdin.pipe(process.stdout);
}

var concat = function() {
    var concat = require("concat-stream")(function(buff) {
        var o = '';
        var s = buff.toString();
        for (var i = s.length - 1; i >= 0; i--)
            o += s[i];
        process.stdout.write(o);
    });

    process.stdin.pipe(concat);
}

var http_server = function() {
    var http = require('http');
    
    var server = http.createServer(function(req, res) {
        if (req.method === 'POST') {
            req.pipe(require('through')(function(data) {
                console.log("Got data " + data);
                this.queue(data.toString().toUpperCase());
            })).pipe(res);
        }
    });

    server.listen(process.argv[2]);
}

var http_client = function() {
    var request = require("request");

    var req = request.post("http://localhost:8000");
    process.stdin.pipe(req).pipe(process.stdout);
}

var websockets = function() {
    var ws = require("websocket-stream");
    var stream = ws("ws://localhost:8000");
    stream.end("hello\n");
}

var html_stream = function() {
    var trumpet = require("trumpet");
    var through_mod = require("through");
    var tr = trumpet();

    loudStream = tr.select('.loud').createStream();
    process.stdin.pipe(tr);

    var through = through_mod(function(data) {
        //console.log("data is" + data);
        this.queue(data.toString().toUpperCase());
    });

    loudStream.pipe(through).pipe(process.stdout);



}

var curr_test = html_stream;


// main -- 
curr_test();
